/* 
 * File:   calTexture.cpp
 * Author: lb
 * 
 * Created on 20 maggio 2016, 1.44
 */

#include "arTexture.h"

arTexture::arTexture() {
    mTexture = NULL;
    mWidth = 0;
    mHeight = 0;
}

void arTexture::init(SDL_Renderer* current_renderer){
    textureRenderer = current_renderer;
}

arTexture::~arTexture() {
    free();
}

bool arTexture::loadFromFile(std::string path){
    free();
    SDL_Texture* newTexture = NULL;
    SDL_Surface* loadedSurface = IMG_Load(path.c_str());
    if(loadedSurface == NULL){
        printf( "non posso caricare l\'immagine %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
    } else {
        SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format,0,0xFF,0XFF));
        newTexture = SDL_CreateTextureFromSurface(textureRenderer,loadedSurface);
        if(newTexture==NULL){
            printf("niente texture \n");
        } else {
            mWidth = loadedSurface->w;
            mHeight = loadedSurface->h;
        }
        SDL_FreeSurface(loadedSurface);
    }
    mTexture = newTexture;
    return mTexture != NULL;
}

void arTexture::simpleRender(int x, int y){
    SDL_Rect renderQuad = {x,y,mWidth,mHeight};
    SDL_RenderCopy(textureRenderer, mTexture, NULL, &renderQuad);
}

void arTexture::render(int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip){
    SDL_Rect renderQuad = {x,y,mWidth,mHeight};
    if(clip!=NULL){
        renderQuad.w = clip->w;
        renderQuad.h = clip->h;
    }
    SDL_RenderCopyEx(textureRenderer, mTexture, clip, &renderQuad, angle, center, flip);
}

void arTexture::setColor(Uint8 red, Uint8 green, Uint8 blue){
    SDL_SetTextureColorMod(mTexture, red, green, blue);
}

void arTexture::setBlendMode(SDL_BlendMode blending){
    SDL_SetTextureBlendMode(mTexture, blending);
}
		
void arTexture::setAlpha(Uint8 alpha){
    SDL_SetTextureAlphaMod(mTexture, alpha);
}

int arTexture::getWidth(){
    return mWidth;
}

int arTexture::getHeight(){
    return mHeight;
}

void arTexture::free(){
    if(mTexture!=NULL){
        SDL_DestroyTexture(mTexture);
        mTexture = NULL;
        mWidth = 0;
        mHeight = 0;
    }
}

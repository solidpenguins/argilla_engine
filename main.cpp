/* 
 * File:   main.cpp
 * Author: lb
 *
 * Created on 20 maggio 2016, 1.08
 */

#include <cstdlib>
#include "arSDL.h"
#include "arTexture.h"
#include "jack.h"
#include "enemyBase.h"

using namespace std;
arSDL s;
arTexture background;

int main(int argc, char** argv) {
    if(s.arInit()){
        background.init(s.getRender());
        background.loadFromFile("assets/background.png");
        jack mainChar(s.getRender(), s.getWindowWidth(), s.getWindowHeight());
        enemyBase firstEnemy(s.getRender(), s.getWindowWidth(), s.getWindowHeight());
        bool quit = false;
        SDL_Event e;
        while(!quit){
            while(SDL_PollEvent(&e)!=0){
                if(e.type == SDL_QUIT){
                    quit == true;
                }
                mainChar.HandleEvent(e);
            }  
            firstEnemy.walk();
            mainChar.walk();
            SDL_SetRenderDrawColor(s.getRender(), 0xFF, 0xFF, 0xFF, 0xFF);
            SDL_RenderClear(s.getRender());
            background.simpleRender(0,0);
            mainChar.update();
            firstEnemy.update();
            SDL_RenderPresent(s.getRender());             
        } 
    } else {
        printf( "Failed to initialize!\n" );
    }
    s.arClose();   
    return 0;
}
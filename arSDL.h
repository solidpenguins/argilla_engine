/* 
 * File:   calSDL.h
 * Author: lb
 *
 * Created on 20 maggio 2016, 1.11
 */

#ifndef ARSDL_H
#define ARSDL_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

class arSDL {
    public:
        arSDL();
        ~arSDL();
        SDL_Renderer* getRender();
        SDL_Window* getWindow();
        bool arInit();
        void arClose();
        int getWindowWidth();
        int getWindowHeight();

    private:
        SDL_Window* arWindow;
        SDL_Renderer* arRender;
        TTF_Font* arFont;
        int sWidth;
        int sHeight;
};

#endif 

/* END IF */
/* 
 * File:   jack.cpp
 * Author: lb
 *
 * Created on 19 maggio 2016, 8.59
 */

#include "jack.h"

using namespace std;



jack::jack(SDL_Renderer* jr, int landWidth, int landHeight){
    mPosX = 20;
	mBasePosY = 250;
    mPosY = mBasePosY;
    mVelX = 0;
    mVelY = 0;
    mWidth = 0;
    mHeight = 0;
    frame = 0;
    flipType = SDL_FLIP_NONE;
    degress = 0;
    isWalking = false;
    isAttacking = false;
	isJumping = false;
    combo = 0;
    landingWidth = landWidth;
    landingHeight = landHeight;
    jTexture.init(jr);
    preload();
}

bool jack::preload(){
    bool success = true;
   
    if(!jTexture.loadFromFile("assets/jack.png")){
        printf("CANT LOAD ANIMATION");
        success = false;
    } else {
        //walk
        jSprite[0].x = 0;
        jSprite[0].y = 0;
        jSprite[0].w = 128;
        jSprite[0].h = 128;
        
        jSprite[1].x = 128;
        jSprite[1].y = 0;
        jSprite[1].w = 128;
        jSprite[1].h = 128;
        
        jSprite[2].x = 256;
        jSprite[2].y = 0;
        jSprite[2].w = 128;
        jSprite[2].h = 128;
        
        jSprite[3].x = 384;
        jSprite[3].y = 0;
        jSprite[3].w = 128;
        jSprite[3].h = 128;

        //squat
        jSprite[4].x = 0;
        jSprite[4].y = 128;
        jSprite[4].w = 128;
        jSprite[4].h = 128;
                
        //attack 1
        jSprite[5].x = 128;
        jSprite[5].y = 128;
        jSprite[5].w = 128;
        jSprite[5].h = 128;
        
        //attack 2
        jSprite[6].x = 256;
        jSprite[6].y = 128;
        jSprite[6].w = 128;
        jSprite[6].h = 128;
        
        //attack 2
        jSprite[7].x = 384;
        jSprite[7].y = 128;
        jSprite[7].w = 128;
        jSprite[7].h = 128;
        
        //attack 3
        jSprite[8].x = 0;
        jSprite[8].y = 256;
        jSprite[8].w = 128;
        jSprite[8].h = 128;
        
        //attack 4
        jSprite[9].x = 128;
        jSprite[9].y = 256;
        jSprite[9].w = 128;
        jSprite[9].h = 128; 
        
    }
    
    return success;
}

char tipoSalto;
void jack::HandleEvent(SDL_Event& e){
    keyPressed = e.key.keysym.sym;
    if(e.type == SDL_KEYDOWN && e.key.repeat == 0){
        switch(e.key.keysym.sym){
            case SDLK_q: 
                isAttacking = true;
                isWalking = false;
                break;
            case SDLK_UP: 
			case SDLK_w:
				mVelY -= SPEED; 
				break;
            case SDLK_DOWN: 
			case SDLK_s:
                squat();
            break;
            
            case SDLK_LEFT: 
			case SDLK_a:
                isAttacking = false;
                isWalking = true;
                mVelX -= SPEED; 
                flipType = SDL_FLIP_HORIZONTAL;
            break;
            case SDLK_RIGHT: 
			case SDLK_d:
                 isAttacking = false;

                isWalking = true;
                mVelX += SPEED;
                flipType = SDL_FLIP_NONE;
				break;
			case SDLK_SPACE:
				jump();
            break;
			case SDLK_1:
				tipoSalto = '1';
				break;
			case SDLK_2:
				tipoSalto = '2';
				break;
        }
    } else if(e.type == SDL_KEYUP && e.key.repeat == 0){
        switch(e.key.keysym.sym){
            case SDLK_q: 
                isAttacking = false;
                stopWalking();
                break;
            case SDLK_UP: 
			case SDLK_w:
                stopWalking();
                break;
            case SDLK_DOWN: 
			case SDLK_s:
				frame=0; 
				break;            
            case SDLK_LEFT: 
			case SDLK_a:
                mVelX += SPEED; 
                stopWalking();
            break;
            
            case SDLK_RIGHT: 
			case SDLK_d:
                mVelX -= SPEED; 
                stopWalking();
            break;
        } 
    }
}

void jack::attack(){
    stopWalking();
    ++combo;
    if(frame+combo>9){
        combo = 0;
    }
    frame = 5 + combo;
}

void jack::squat(){
    stopWalking();
    frame = 4;
}

int jack::currentFrame(){
    return frame;
}

void jack::walk(){
    if(isWalking){
        ++frame;
        if(frame>3){
            frame=0;
        }
    }
    if(isAttacking){
        ++combo;
        frame = 4;
        if(frame+combo>9){
            combo = 0;
        }
        frame = 4 + combo;
    }
    mPosX +=mVelX;
    if((mPosX<0) || (mPosX + mWidth > landingWidth)){
        mPosX -= mVelX;
    }
}

void jack::stopWalking(){
    isWalking = false;
    frame = 0;
}

void jack::move(){

    mPosX += mVelX;
    if((mPosX < 0) || (mPosX + mWidth > landingWidth)){
        mPosX -= mVelX;
    }
    mPosY += mVelY;
    if((mPosY < 0) || (mPosY + mHeight > landingHeight)){
        mPosY -= mVelY;
    }
}

float jumpVel = 12;
float gravity = 0.5f;
void jack::jump()
{
	if (!isJumping && (mPosY == mBasePosY))
		isJumping = true;

	if (isJumping)
	{
		mPosY -= jumpVel;
		jumpVel -= gravity;
	}

	if (mPosY == mBasePosY)
	{
		jumpVel = 12;
		isJumping = false;
	}
}

void jack::render(double angle, SDL_Point* center){
    SDL_Rect* currentClip = &jSprite[currentFrame()];
    if(currentClip!=NULL){
        mWidth = currentClip->w;
        mHeight = currentClip->h;
    }
    jTexture.render(mPosX,mPosY,currentClip,angle,center,flipType);
}

void jack::clear(){
    jTexture.free();
}

void jack::update(){
	if (mPosY != mBasePosY)
		jump();
    render(0.0,NULL);
}



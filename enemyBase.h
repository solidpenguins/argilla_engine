/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   enemyBase.h
 * Author: lb
 *
 * Created on 23 maggio 2016, 12.34
 */

#ifndef ENEMYBASE_H
#define ENEMYBASE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <cstdlib>
#include "arTexture.h"

class enemyBase {
    public:
        static const int SPEED = 10;
        enemyBase(SDL_Renderer* jr, int landWidth, int landHeight);
        ~enemyBase();
        void handleEvent( SDL_Event& e );
        void move(SDL_Rect& wall);
        bool preload();
        void HandleEvent(SDL_Event& e);
        void walk();   
        void stopWalking();
        int currentFrame();    
        void render(double angle, SDL_Point* center);
        void clear();
        void update();
        void squat();
        void attack();
                
    private:
        int mPosX, mPosY;
        int mVelX, mVelY;
        SDL_Rect mCollider;
        int mWidth,mHeight; 
        int landingWidth,landingHeight; 
        int frame;
        SDL_RendererFlip flipType;
        int degress;
        SDL_Rect jSprite[10];
        int keyPressed;
        bool isWalking, isAttacking;
        int combo;
        arTexture jTexture;
};

#endif /* ENEMYBASE_H */


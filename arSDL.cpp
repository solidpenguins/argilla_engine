/* 
 * File:   arSDL.cpp
 * Author: lb
 * 
 * Created on 20 maggio 2016, 1.11
 */

#include "arSDL.h"
#include <stdio.h>

arSDL::arSDL() {
    arRender = NULL;
    arWindow = NULL;
    sWidth = 640;
    sHeight = 480;
}

arSDL::~arSDL() {}

SDL_Renderer* arSDL::getRender(){
    return arRender;
}

SDL_Window* arSDL::getWindow(){
    return arWindow;
}

int arSDL::getWindowWidth(){
    return sWidth;
}

int arSDL::getWindowHeight(){
    return sHeight;
}

bool arSDL::arInit(){
    bool success = true;
    if(SDL_Init(SDL_INIT_VIDEO)<0){
        printf("SDL ERROR: %s/n",SDL_GetError());
        success = false;
    } else {      
        if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY,"1")){
            printf("Linear Texture not enabled\n");
        }
        arWindow = SDL_CreateWindow("Argilla Engine", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,  sWidth, sHeight, SDL_WINDOW_SHOWN );
        if(arWindow == NULL){
            printf("SDL WINDOW ERROR: %s/n",SDL_GetError());
            success = false;
        } else {        
            arRender = SDL_CreateRenderer(arWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
            if(arRender == NULL){
                printf("SDL Renderer error");
                success = false;
            } else {
                SDL_SetRenderDrawColor(arRender, 0xFF, 0xFF, 0xFF, 0xFF );
                int imgFlags = IMG_INIT_PNG;
                if(!(IMG_Init(imgFlags) & imgFlags)){
                    printf( "SDL_image error! SDL_image Error: %s\n", IMG_GetError() );
                    success = false;
                }
                if( TTF_Init() == -1 ){
                    printf( "SDL_ttf error SDL_ttf Error: %s\n", TTF_GetError() );
                    success = false;
                }
            }
        }
    }
    return success;  
}

void arSDL::arClose(){
    SDL_DestroyRenderer(arRender);
    arWindow = NULL;
    SDL_DestroyWindow(arWindow);
    arRender = NULL;
    IMG_Quit();
    SDL_Quit();
    TTF_Quit();
}
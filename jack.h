/* 
 * File:   jack.h
 * Author: lb
 *
 * Created on 19 maggio 2016, 8.57
 */

#ifndef JACK_H
#define JACK_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <cstdlib>
#include "arTexture.h"


class jack{
    public:
        static const int SPEED = 10;
        jack(SDL_Renderer* jr, int landWidth, int landHeight);
        bool preload();
        void HandleEvent(SDL_Event& e);
        void walk();   
        void stopWalking();
        void move();
        int currentFrame();    
        void render(double angle, SDL_Point* center);
        void clear();
        void update();
        void squat();
        void attack();
		void jump();

    private:
        int mPosX, mPosY, mBasePosY;
        int mVelX, mVelY;
        int mWidth,mHeight; 
        int landingWidth,landingHeight; 
        int frame;
        SDL_RendererFlip flipType;
        int degress;
        SDL_Rect jSprite[10];
        int keyPressed;
        bool isWalking, isAttacking, isJumping;
        int combo;
        arTexture jTexture;
};

#endif /* JACK_H */
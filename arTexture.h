/* 
 * File:   calTexture.h
 * Author: lb
 *
 * Created on 20 maggio 2016, 1.44
 */

#ifndef ARTEXTURE_H
#define ARTEXTURE_H
#include "arSDL.h"
#include <cstdlib>
#include <string>

class arTexture {
    public:
        arTexture();
        ~arTexture();
        void init(SDL_Renderer* current_renderer);
        bool loadFromFile(std::string path);
        void free();
        void setColor(Uint8 red, Uint8 green, Uint8 blue);
        void setBlendMode(SDL_BlendMode bleding);
        void setAlpha(Uint8 alpha);
        void simpleRender(int x, int y);
        void render(int x, int y, SDL_Rect* clip = NULL, double angle=0.0, SDL_Point* center=NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);  
        int getWidth();
        int getHeight();
    private:
        SDL_Renderer* textureRenderer;
        SDL_Texture* mTexture;
        int mWidth;
        int mHeight;
};

#endif /* ARTEXTURE_H */

